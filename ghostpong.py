
import os  
import random
import turtle  
import time


# First, we will create screen  
screen_1 = turtle.Screen()  
screen_1.title("Ping-Pong Game")  
screen_1.bgcolor("Yellow")  
screen_1.setup(width = 1050, height = 650)  
#pozadí
scren_2 = turtle.Turtle()
turtle.register_shape('/home/sozonovs/Downloads/hra/arena.gif')
scren_2.shape('/home/sozonovs/Downloads/hra/arena.gif')

# Net
net = turtle.Turtle()
turtle.register_shape('/home/sozonovs/Downloads/hra/tree.gif')
net.speed(0)
net.shape('/home/sozonovs/Downloads/hra/tree.gif') 
net.shapesize(stretch_wid = 20, stretch_len = 2)  
net.penup()  
net.goto(0, -205)  

# Left paddle  
left_paddle = turtle.Turtle()  
left_paddle.speed(5)  
turtle.register_shape('/home/sozonovs/Downloads/hra/duch.gif')
left_paddle.shape('/home/sozonovs/Downloads/hra/duch.gif') 
left_paddle.shapesize(stretch_wid = 2.5, stretch_len = 2.5)
left_paddle.color("black")  
left_paddle.penup()  
left_paddle.goto(-450, -240)  
   
   
# Right paddle  
right_paddle = turtle.Turtle()  
right_paddle.speed(5)  
turtle.register_shape('/home/sozonovs/Downloads/hra/duchl.gif')
right_paddle.shape('/home/sozonovs/Downloads/hra/duchl.gif') 
right_paddle.shapesize(stretch_wid = 2.5, stretch_len = 2.5) 
right_paddle.color("Black")
right_paddle.penup()  
right_paddle.goto(200, -240)  

   
# Ball of circle shape  
hit_ball = turtle.Turtle()  
hit_ball.speed(45)  
turtle.register_shape('/home/sozonovs/Downloads/hra/ball.gif')
hit_ball.shape('/home/sozonovs/Downloads/hra/ball.gif')
hit_ball.shapesize(stretch_wid = 2.5, stretch_len = 2.5) 
hit_ball.penup()  
hit_ball.goto(0, 100)  
hit_ball.dx = 9
hit_ball.dy = -18
   
   
# Now, we will initialize the score  
left_player = 0  
right_player = 0  
   
   
# Displaying of the score  
sketch_1 = turtle.Turtle()  
sketch_1.speed(0)  
sketch_1.color("white")  
sketch_1.penup()  
sketch_1.hideturtle()  
sketch_1.goto(0, 260)  
sketch_1.write("Left Player : 0    Right Player: 0",  
             align = "center", font = ("Thirteen Pixel Fonts", 24, "normal"))  
   
   
# Implementing the functions for moving paddle vertically  
def paddle_L_up():  
    x = left_paddle.xcor()  
    x += 40
    left_paddle.setx(x)  
    left_paddle.clear()
    left_paddle.shape('/home/sozonovs/Downloads/hra/duch.gif') 
   
def paddle_L_down():  
    x = left_paddle.xcor()  
    x -= 40
    left_paddle.setx(x)  
    left_paddle.clear()
    left_paddle.shape('/home/sozonovs/Downloads/hra/duchl.gif')
   
def paddle_R_up():  
    x = right_paddle.xcor()  
    x += 40
    right_paddle.setx(x)  
    right_paddle.clear()
    right_paddle.shape('/home/sozonovs/Downloads/hra/duch.gif') 
    
def paddle_R_down():  
    x = right_paddle.xcor()  
    x -= 40
    right_paddle.setx(x)  
    right_paddle.clear()
    right_paddle.shape('/home/sozonovs/Downloads/hra/duchl.gif') 

sketch_2 = turtle.Turtle()  
sketch_2.speed(0)  
sketch_2.color("white")  
sketch_2.penup()  
sketch_2.hideturtle()  
    
# Then, binding the keys for moving the paddles up and down.   
screen_1.listen()  
screen_1.onkeypress(paddle_L_up, "d")  
screen_1.onkeypress(paddle_L_down, "a")  
screen_1.onkeypress(paddle_R_up, "Right")  
screen_1.onkeypress(paddle_R_down, "Left") 
    
while True:  
    screen_1.update()  
    hit_ball.setx(hit_ball.xcor() + hit_ball.dx)  
    hit_ball.sety(hit_ball.ycor() + hit_ball.dy)  
     
    
   # Check all the borders  
    if hit_ball.xcor() > 525:  
        hit_ball.setx(524)  
        hit_ball.dx *= -1 
    
    if hit_ball.xcor() < -525:  
        hit_ball.setx(-524)  
        hit_ball.dx *= -1 
        
    if hit_ball.ycor() > 325:  
        hit_ball.sety(324)  
        hit_ball.dy *= -1 
   

#Dolní hranice
    if hit_ball.ycor() < -340 and hit_ball.xcor() > 1:  
        left_player += 1
        sketch_1.clear()
        sketch_1.write("Left_player : {}    Right_player: {}".format(
                                 left_player, right_player), align="center",
                                 font=("Thirteen Pixel Fonts", 24, "normal"))
        left_paddle.goto(-450, -240)  
        right_paddle.goto(200, -240)  
        sketch_2.goto(0, 0) 
        sketch_2.hideturtle()  
        sketch_2.write("{} : {}".format(
                                 left_player, right_player), align="center",
                                 font=("Thirteen Pixel Fonts", 100, "normal")) 
        time.sleep(1) 
        sketch_2.clear()
        hit_ball.goto(0, 100)
        hit_ball.dx = 9
        hit_ball.dy = -18
        
    if hit_ball.ycor() < -340 and hit_ball.xcor() < 1:  
        right_player += 1
        sketch_1.clear()
        sketch_1.write("Left_player : {}    Right_player: {}".format(
                                 left_player, right_player), align="center",
                                 font=("Thirteen Pixel Fonts", 24, "normal"))
        left_paddle.goto(-450, -240)  
        right_paddle.goto(200, -240)  
        sketch_2.goto(0, 0)
        sketch_2.hideturtle()  
        sketch_2.write("{} : {}".format(
                                 left_player, right_player), align="center",
                                 font=("Thirteen Pixel Fonts", 100, "normal")) 
        time.sleep(1)
        sketch_2.clear()
        hit_ball.goto(0, 100)
        hit_ball.dx = 9
        hit_ball.dy = -18
                   
    if (hit_ball.ycor() < -180 and  
                        hit_ball.xcor() > -210) and (hit_ball.xcor() < right_paddle.xcor() + 10 and  
                        hit_ball.xcor() > right_paddle.xcor() - 10):  
                        right_paddle.clear()
                        right_paddle.shape('/home/sozonovs/Downloads/hra/duchl.gif') 
                        hit_ball.sety(-180)
                        hit_ball.dy *= -1.125
                        hit_ball.dx *= -1
    if (hit_ball.ycor() < -180 and  
                       hit_ball.ycor() > -210) and (hit_ball.xcor() < left_paddle.xcor() + 10 and  
                       hit_ball.xcor() > left_paddle.xcor() - 10):  
                       left_paddle.clear()
                       left_paddle.shape('/home/sozonovs/Downloads/hra/duch.gif')
                       hit_ball.sety(-180)
                       hit_ball.dy *= -1.125
                       hit_ball.dx *= -1

    if (hit_ball.ycor() < -210 and  
                        hit_ball.xcor() > -230) and (hit_ball.xcor() < right_paddle.xcor() + 50 and  
                        hit_ball.xcor() > right_paddle.xcor() - 50):  
                        right_paddle.clear()
                        right_paddle.shape('/home/sozonovs/Downloads/hra/duchl.gif') 
                        hit_ball.sety(-180)
                        hit_ball.dy *= -1
                        hit_ball.dx *= -1.2
          
    if (hit_ball.ycor() < -210 and  
                       hit_ball.ycor() > -230) and (hit_ball.xcor() < left_paddle.xcor() + 50 and  
                       hit_ball.xcor() > left_paddle.xcor() - 50):  
                       left_paddle.clear()
                       left_paddle.shape('/home/sozonovs/Downloads/hra/duch.gif')
                       hit_ball.sety(-180)
                       hit_ball.dy *= -1
                       hit_ball.dx *= -1.2
